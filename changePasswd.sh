#!/bin/sh
egrep "^$1" /etc/passwd >& /dev/null
	if [ $? -ne 0 ]
	# 如果不存在就添加用户，下面和addUser的流程一致
	then
		echo 'User not found... Adding user '$1' ...'
		useradd $1
	fi
	if [ ! -d '/opt/'$1 ]; then
		echo 'Directory not exists... Creating directory /opt/share/'$1'/ ...'
		mkdir '/opt/'$1
	fi
	echo 'Samba config changing....'

	echo '['$1']'>>/etc/samba/smb.conf
	echo '  path = /opt/'$1>>/etc/samba/smb.conf
	echo '  available = yes'>>/etc/samba/smb.conf
	echo '  browsable = yes'>>/etc/samba/smb.conf
	echo '  writable = yes'>>/etc/samba/smb.conf
	echo '  valid users = '$1>>/etc/samba/smb.conf
	echo '  public = yes'>>/etc/samba/smb.conf
	echo '  read only = no'>>/etc/samba/smb.conf
	echo ''	>>/etc/samba/smb.conf
	
	echo 'Samba config changed.'

	echo 'Restart samba service...'
	sudo  /etc/init.d/smbd restart

	echo 'Setting samba password...'
	(echo "$2"; echo "$2") | smbpasswd  -a "$1"

	echo 'Done.'

	else

	# 这里是用户如果存在的话，就修改密码,把传入的第二个变量传给指令
		echo 'Setting samba password...'
		(echo "$2"; echo "$2") | smbpasswd "$1"
	fi
