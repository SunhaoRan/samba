#!/bin/sh

# 判断是否有用户
egrep "^$1" /etc/passwd >& /dev/null
	if [ $? -ne 0 ] #如果不存在用户
	then
		echo 'User not found... Adding user '$1' ...'
		useradd $1  # 创建用户
		if [ ! -d '/opt/'$1 ]; then   # 创建共享用的文件夹
			echo 'Directory not exists... Creating directory /opt/share/'$1'/ ...'
			mkdir '/opt/'$1
			echo 'Samba config changing....'
		fi

		# 下面是在samba中添加8行配置
		echo '['$1']'>>/etc/samba/smb.conf
		echo '  path = /opt/'$1>>/etc/samba/smb.conf
		echo '  available = yes'>>/etc/samba/smb.conf
		echo '  browsable = yes'>>/etc/samba/smb.conf
		echo '  writable = yes'>>/etc/samba/smb.conf
		echo '  valid users = '$1>>/etc/samba/smb.conf
		echo '  public = yes'>>/etc/samba/smb.conf
		echo '  read only = no'>>/etc/samba/smb.conf
		echo ''	>>/etc/samba/smb.conf

		echo 'Samba config changed.'

		# 重启一下samba服务
		echo 'Restart samba service...'
		sudo  /etc/init.d/smbd restart

		# 设定samba用户的密码
		echo 'Setting samba password...'
		(echo "$2"; echo "$2") | smbpasswd -a "$1"

		echo 'Done.'
	else
		echo 'User' $1 'already exists... Operation failed...'
	fi