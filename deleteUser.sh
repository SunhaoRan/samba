#!/bin/sh
egrep "^$1" /etc/passwd >& /dev/null
	if [ $? -ne 0 ]
	# 如果用户不存在，就别删了
	then
        echo 'User not found...'
	else
	# 用户存在的话，那删掉用户，但是保留文件
		echo 'User '$1' deleted.'
		userdel -r $1
		smbpasswd -x $1
		# samba的配置在add的过程添加了8行，这里就删掉
		sed -i '/\<'$1'\>/,+8d' /etc/samba/smb.conf
	fi
